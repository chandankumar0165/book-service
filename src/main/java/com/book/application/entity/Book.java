package com.book.application.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.book.application.helper.DateHandler;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

@Entity
@Data
@Cache (usage=CacheConcurrencyStrategy.TRANSACTIONAL)
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "category")
	private String category;
	
	@Column(name = "isbn")
	private String isbn;
	
	@Column(name = "number_of_pages")
	private Integer numberOfPages;
	
	@Column(name = "year")
	private String year;
	
	@Column(name = "author")
	private String author;
	
	@Column(name = "publisher")
	private String publisher;
	
	 @JsonDeserialize(using = DateHandler.class)
	@Column(name = "release_date")
	private Date releaseDate;
	
	@Column(name = "price")
	private Double price;

}

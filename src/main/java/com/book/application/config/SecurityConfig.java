package com.book.application.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	    @Value("${spring.security.user.name}")
		private String username;
		@Value("${spring.security.user.password}")
		private String password;
		
		  
		
	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	        String rawPassword = password;
	        String encodedPassword = encoder.encode(rawPassword);
	        
	        auth.inMemoryAuthentication()
	            .passwordEncoder(new BCryptPasswordEncoder())
	                .withUser(username)
	                .password(encodedPassword)
	                .roles("USER")
	            ;
	    }
	 
	 
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	    	http.csrf().disable();
	        http.authorizeRequests()
	        .antMatchers("/*").hasRole("USER")
	            .anyRequest().authenticated()
	            .and()
	            .formLogin().and()
	            .httpBasic();
	        
	    }
}

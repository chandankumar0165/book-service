package com.book.application.dto;

import lombok.Builder;

@Builder
public class ResponseDTO {
	private String code;
	@Builder.Default
	private String message = "Success!";

	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

package com.book.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.events.Event.ID;

import com.book.application.dto.ResponseDTO;
import com.book.application.entity.Book;
import com.book.application.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BookService {
	
	@Autowired
	private BookRepository bookRepository ;
	
	public ResponseDTO save(Book body) {
			bookRepository.save(body);
			log.info("book saved");
			return ResponseDTO.builder().code("101").build();		
	}

	public ResponseDTO update(Long id, Book body) {
		Optional<Book> bookOptional = bookRepository.findById(id);
		
		if(bookOptional.isPresent()) {
			body.setId(bookOptional.get().getId());
		}
		bookRepository.save(body);
		log.info("book updated");
		return ResponseDTO.builder().code("101").build();
	}

	public ResponseDTO patch(Long id, Book body) {
		Optional<Book> bookOptional = bookRepository.findById(id);
		
		if(bookOptional.isPresent()) {
			Book book = bookOptional.get();
			if(!(body.getAuthor()==null) && !body.getAuthor().isEmpty() && !body.getAuthor().equals("")) {
				book.setAuthor(body.getAuthor());
			}
			if(!(body.getCategory()==null) && !body.getCategory().isEmpty() && !body.getCategory().equals("")) {
				book.setCategory(body.getCategory());
			}
			if(!(body.getPrice()==null)) {
				book.setPrice(body.getPrice());
			}
			if(!(body.getIsbn()==null)) {
				book.setIsbn(body.getIsbn());
			}
			if(!(body.getPublisher()==null) && !body.getPublisher().isEmpty() && !body.getPublisher().equals("")) {
				book.setPublisher(body.getPublisher());
			}
			if(!(body.getReleaseDate()==null)) {
				book.setReleaseDate(body.getReleaseDate());
			}
			if(!(body.getTitle()==null) &&!body.getTitle().isEmpty() && !body.getTitle().equals("")) {
				book.setTitle(body.getTitle());
			}
			if(!(body.getYear()==null) &&!body.getYear().isEmpty() && !body.getYear().equals("")) {
				book.setYear(body.getYear());
			}
			if(!(body.getNumberOfPages()==null)) {
				book.setNumberOfPages(body.getNumberOfPages());
			}
			bookRepository.save(body);
			log.info("book updated");
			return ResponseDTO.builder().code("101").build();
		}
		log.info("book not found");
		return ResponseDTO.builder().code("404").message("Book not found").build();
		
		
	}

	public ResponseDTO delete(Long id) {
		Optional<Book> bookOptional = bookRepository.findById(id);
		if(bookOptional.isPresent()) {
			bookRepository.deleteById(id);
			log.info("book deleted");
			return ResponseDTO.builder().code("101").build();
		}
		log.info("book not found");
		return ResponseDTO.builder().code("404").message("Book not found").build();
		
	}
	
	public ResponseEntity<List<Book>> findAll() {
		Iterable<Book> bookList = bookRepository.findAll();
		List<Book> result = new ArrayList<Book>();
		bookList.forEach(result::add);
		return ResponseEntity.ok(result);
	}

	public ResponseEntity<Book> find(Long id) {
		Optional<Book> book = bookRepository.findById(id);
		return ResponseEntity.ok(book.get());
	}

}
